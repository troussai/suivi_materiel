const pjson = require('../../../package.json');

module.exports = {
  "swagger": "2.0",
  "info": {
      "version": pjson.version,
      "title": "Suivi de materiel Swagger interface",
      "description": "Swagger interface for suivi de materiel back-end side",
  },
  "host": "localhost:3000",
  "basePath": "/api",
  "tags": [
  ],
  "consumes": [
      "application/json"
  ],
  "produces": [
      "application/json"
  ],
  "paths": { },
  "definitions": { }
}